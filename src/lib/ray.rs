use crate::colour::Colour;
use crate::hittable::Hittable;
use crate::point3::Point3;
use crate::sphere::Sphere;
use crate::vec3::Vec3;

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    pub origin: Point3,
    pub direction: Vec3,
}

impl Ray {
    pub fn new(origin: Point3, direction: Vec3) -> Self {
        Ray {
            origin,
            direction,
        }
    }

    pub fn at(&self, t: f64) -> Point3 {
        return self.origin + t * self.direction;
    }

    pub fn color(&self) -> Colour {
        // TODO: Remove sphere from here and create a list of elements on screen to check the ray against
        let sphere: Sphere = Sphere::new(Point3::new(0.0, 0.0, -1.0), 0.5);
        let hit_root = sphere.hit(&self);
        if hit_root > 0.0 {
            let normal: Vec3 = (self.at(hit_root) - sphere.center).unit_vector();
            return 0.5 * Colour::new(normal.x + 1.0, normal.y + 1.0, normal.z + 1.0);
        }
        let t: f64 = 0.5 * (self.direction.unit_vector().y + 1.0);
        return (1.0 - t) * Colour::new(1.0, 1.0, 1.0 ) + t * Colour::new( 0.5, 0.7, 1.0 );
    }
}