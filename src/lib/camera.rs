use crate::point3::Point3;
use crate::vec3::Vec3;

#[derive(Debug, Copy, Clone)]
pub struct Camera {
    pub viewport_width: f64,
    pub viewport_height: f64,
    pub focal_length: f64,
    pub origin: Point3,
    pub horizontal: Vec3,
    pub vertical: Vec3,
    pub lower_left_corner: Vec3,
}

impl Camera {
    pub fn new(viewport_width: f64, viewport_height: f64, focal_length: f64, origin: Point3) -> Self {
        let horizontal: Vec3 = Vec3::new(viewport_width, 0.0, 0.0 );
        let vertical: Vec3 = Vec3::new(0.0, viewport_height, 0.0 );
        Camera {
            viewport_width,
            viewport_height,
            focal_length,
            origin,
            horizontal,
            vertical,
            lower_left_corner: origin - horizontal / 2.0 - vertical / 2.0 - Vec3::new(0.0, 0.0, focal_length),
        }
    }
}