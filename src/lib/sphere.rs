use crate::hittable::Hittable;
use crate::point3::Point3;
use crate::ray::Ray;
use crate::vec3::Vec3;

pub struct Sphere {
    pub center: Point3,
    pub radius: f64,
}

impl Sphere {
    pub fn new(center: Point3, radius: f64) -> Self {
        Sphere {
            center,
            radius
        }
    }
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray) -> f64 {
        let origin_center: Vec3 = ray.origin - self.center;

        let a: f64 = ray.direction.length_squared();
        let half_b: f64 = origin_center.dot(ray.direction);
        let c: f64 = origin_center.length_squared() - self.radius * self.radius;
        let discriminant: f64 = half_b * half_b - a * c;

        return if discriminant < 0.0 {
            -1.0
        } else {
            (-half_b - discriminant.sqrt()) / a
        }
    }
}