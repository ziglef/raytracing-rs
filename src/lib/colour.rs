use core::fmt;
use std::ops;

// We are representing the channels as a percentage
pub struct Colour {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

impl Colour {
    pub fn new(r: f64, g: f64, b: f64) -> Self {
        Colour {
            r,
            g,
            b,
        }
    }
    pub const CHANNEL_MAX: usize = 255;
}

// Colour + Colour
impl ops::Add<Colour> for Colour {
    type Output = Colour;

    fn add(self, other: Colour) -> Self::Output {
        Colour {
            r: self.r + other.r,
            g: self.g + other.g,
            b: self.b + other.b,
        }
    }
}

// f64 * Colour
impl ops::Mul<Colour> for f64 {
    type Output = Colour;

    fn mul(self, other: Colour) -> Self::Output {
        Colour {
            r: other.r * self,
            g: other.g * self,
            b: other.b * self,
        }
    }
}

// Display implementation
impl fmt::Display for Colour {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {} {}", ((Colour::CHANNEL_MAX as f64 + 0.999) * self.r) as usize, ((Colour::CHANNEL_MAX as f64 + 0.999) * self.g) as usize, ((Colour::CHANNEL_MAX as f64 + 0.999) * self.b) as usize)
    }
}
