use rtlib::camera::Camera;
use rtlib::colour::Colour;
use rtlib::point3::Point3;
use rtlib::ray::Ray;

// Image
static IMG_RATIO: f64 = 16.0 / 9.0;
static IMG_WIDTH: usize = 1280;
static IMG_HEIGHT: usize = (IMG_WIDTH as f64 / IMG_RATIO) as usize;

fn main() {
    // Create a Camera
    let camera: Camera = Camera::new(2.0 * IMG_RATIO, 2.0, 1.0, Point3::new(0.0, 0.0, 0.0));

    // Rendering
    // HEADERS
    println!("P3\n{} {}\n{}", IMG_WIDTH, IMG_HEIGHT, Colour::CHANNEL_MAX);

    // PIXELS
    eprintln!("Starting Render!");
    for y in (0..IMG_HEIGHT).rev() {
        eprintln!("Scanlines remaining: {}", y);
        for x in 0..IMG_WIDTH {
            let u: f64 = x as f64 / (IMG_WIDTH-1) as f64;
            let v: f64 = y as f64 / (IMG_HEIGHT-1) as f64;

            let r: Ray = Ray::new(camera.origin, camera.lower_left_corner + u*camera.horizontal + v*camera.vertical - camera.origin);
            println!("{}", r.color());
        }
    }
    eprintln!("Done!");
}
